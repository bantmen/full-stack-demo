import asyncio
import aiohttp
from flask import Flask, render_template, request

from utils import merge_hotels, list_to_dict_key

app = Flask(__name__)
loop = asyncio.get_event_loop()

endpoint = "https://experimentation.snaptravel.com/interview/hotels"
# Note: The cache does not have TTL.
# Consider using a better solution if it starts getting unwieldy.
merged_hotels_cache = {}


async def network_fetch_hotels(payload, num_requests):
    results = []
    async with aiohttp.ClientSession(loop=loop) as session:

        def create_task():
            return asyncio.ensure_future(
                session.post(endpoint, json=payload, timeout=10)
            )

        # Make the requests in parallel
        tasks = [create_task() for i in range(num_requests)]
        # Collect the results
        for task in tasks:
            resp = await task
            if resp.status == 200:
                results.append(await resp.json())
            else:
                print("Non 200 response - received %s" % resp.status)
    return results


def get_hotels(payload):
    cached_val = merged_hotels_cache.get(list_to_dict_key(payload.values()), None)
    if cached_val is not None:
        return cached_val

    results = loop.run_until_complete(
        network_fetch_hotels(payload=payload, num_requests=2)
    )
    if len(results) < 2:
        return []
    hotels1 = results[0]["hotels"]  # Snaptravel
    hotels2 = results[1]["hotels"]  # Hotel.com
    merged_hotels = merge_hotels(hotels1, hotels2)
    merged_hotels = sorted(merged_hotels, key=lambda d: d["id"])

    merged_hotels_cache[list_to_dict_key(payload.values())] = merged_hotels
    return merged_hotels


@app.route("/form", methods=["POST"])
def form_submit():
    payload = {}
    payload["city"] = request.form["city"]
    payload["checkin"] = request.form["checkin"]
    payload["checkout"] = request.form["checkout"]
    payload["provider"] = "snaptravel"
    # Populate default values for convenience
    for key in payload:
        if payload[key] == "":
            payload[key] = "default-%s" % key

    hotels = get_hotels(payload)
    if not hotels:
        return "Error", 500
    return render_template("hotels.html", data=hotels)


@app.route("/", methods=["GET"])
def main():
    return render_template("form.html")
