# Joins 2 lists of hotels based on ids, filtering out
# hotels that are not present in both lists.
# For each hotel, preserves the price from both lists.
def merge_hotels(hotels1, hotels2):
    hotel1_ids = {hotel["id"] for hotel in hotels1}
    hotel2_ids = {hotel["id"] for hotel in hotels2}
    common_ids = hotel1_ids.intersection(hotel2_ids)
    id_to_hotel = {}
    for hotel in hotels1:
        if hotel["id"] in common_ids:
            hotel["price1"] = hotel["price"]
            id_to_hotel[hotel["id"]] = hotel
    for hotel in hotels2:
        if hotel["id"] in common_ids:
            id_to_hotel[hotel["id"]]["price2"] = hotel["price"]
    return id_to_hotel.values()


def list_to_dict_key(l):
    # Sort to make the key consistent
    return tuple(sorted(l))
